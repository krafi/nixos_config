#!/bin/bash

# Get the full path and filename of the currently running script
fullpath="$(realpath "$0")"

# Get the filename with extension
filename="$(basename "$fullpath")"

echo "The name of the currently running script is: $filename"
