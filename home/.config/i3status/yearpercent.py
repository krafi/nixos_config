
import datetime

# Calculate the percentage of the year that has passed
today = datetime.date.today()
year_start = datetime.date(today.year, 1, 1)
days_passed = (today - year_start).days
total_days_in_year = 365 if not today.year % 4 == 0 or (today.year % 100 == 0 and not today.year % 400 == 0) else 366  # Check for leap year

percentage = (days_passed / total_days_in_year) * 100

# Print the percentage with a symbol (you can customize this)
print(f"🐧{percentage:.2f}%")
