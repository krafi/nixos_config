#!/bin/bash

# Get the current year and day of the year
year=$(date +%Y)
day_of_year=$(date +%j)

# Check if it's a leap year
if (( (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) )); then
  total_days_in_year=366
else
  total_days_in_year=365
fi

# Calculate the percentage
percentage=$(bc -l <<< "scale=2; ($day_of_year / $total_days_in_year) * 100")

# Print the percentage with a symbol (you can customize this)
echo "🐧$percentage%"

