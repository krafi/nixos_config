# alias

alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ram="~/./swapf"
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'
alias jctl="journalctl -p 3 -xb"



#zplug "dracula/zsh", as:theme
#force_color_prompt = yes
alias darkmpv=cat " mpv --vf=negate --hwdec=no "

function sshot(){


install_packages() {
    if [ -f /etc/lsb-release ]; then
        sudo apt-get update
        sudo apt-get install -y scrot feh
    elif [ -f /etc/arch-release ]; then
        sudo pacman -Sy scrot feh
    elif [ -f /etc/redhat-release ]; then
        sudo dnf install -y scrot feh
    else
        echo "Unsupported distribution. Please install scrot and feh manually. If you are certain that these packages are already installed, you can safely ignore this message." && mkdir -p ~/.tmp_screenshot && cd  ~/.tmp_screenshot && scrot -s ~/.tmp_screenshot/temp.png && mv ~/.tmp_screenshot/temp.png ~/.tmp_screenshot && cd -

    fi
}

if ! command -v scrot &> /dev/null || ! command -v feh &> /dev/null; then
    echo "scrot and/or feh are not installed."

    install_packages

    if [ $? -eq 0 ]; then
        echo "scrot and feh have been installed."
    else
        echo "Failed to install scrot and feh."
    fi
else
    echo "scrot and feh are already installed." && mkdir -p ~/.tmp_screenshot && cd  ~/.tmp_screenshot && scrot -s ~/.tmp_screenshot/temp.png && mv ~/.tmp_screenshot/temp.png ~/.tmp_screenshot/temp_000.png && feh --borderless --title 'Image Viewer' temp_000.png && cd -
fi

}



function man-python-venv(){
	echo "nix-shell -p python3 -p python3Packages.virtualenv --run "python3 -m venv venv""
    echo "source venv/bin/activate"
    echo "deactivate"
}
function zsh-nix-update(){
	nix channel --update && sudo nixos-rebuild switch --upgrade
}
function zsh-nix-clean-up(){
	sudo nix-store --optimise && sudo nix-store --gc 
}
function zsh-nix-clean-gen(){
	sudo nix-collect-garbage -d
}
function zsh-nix-gen(){
	sudo nix-env --list-generations --profile /nix/var/nix/profiles/system
}
function pnix() {
    read -rsn1 -p "Press 1 (or 'a') to edit NixOS config, 2 (or 's') to rebuild, or Enter to exit: " choice
    case "$choice" in
        1|a)
            sudo cudatext /etc/nixos/configuration.nix
            ;;
        2|s)
            sudo nixos-rebuild switch
            ;;
        *)
            echo "Exiting..."
            ;;
    esac
}
function b(){
pkexec echo "$1" > /sys/class/backlight/acpi_video0/brightness
}
function hex-encode()
{
  echo "$@" | xxd -p
}

function hex-decode()
{
  echo "$@" | xxd -p -r
}

function rot13()
{
  echo "$@" | tr 'A-Za-z' 'N-ZA-Mn-za-m'
}

function by() {
	echo "gcc compiling your c++...
	"
	g++ "$1" &&
	       	./a.out &&
	echo "------EOL-------"
}
function py(){
	echo "SOL" &&
	py "$1" &&
		echo "EOL"

}



funtion pnix() {
    read -rsn1 -p "Press 1 (or 'a') to edit NixOS config, 2 (or 's') to rebuild, or Enter to exit: " choice
    case "$choice" in
        1|a)
            sudo cudatext /etc/nixos/configuration.nix
            ;;
        2|s)
            sudo nixos-rebuild switch
            ;;
        *)
            echo "Exiting..."
            ;;
    esac
}


function dnfu(){
	sudo dnf update --refresh

}
function prizrak_zakar(){
cd ~/web/backend-krafiinfo.gitlab.io/HERE/prizrak/ &&
git add . &&
git commit -m "update" &&
git push
}

function prizrak(){
cd ~/web/backend-krafiinfo.gitlab.io/HERE/prizrak/ &&
ghost restart &&
ghost ls
}
function lg() {
    git add .
    git commit -m "$1"
    git push
            
}


function 20m() {
   sleep 20m  && espeak -s 150 "sir your 20 min is over" && xcowsay "20 min over" && 20m
}

function webb(){
cd ~/web/backend-of-kazi-ar-rafi.com &&

echo "./static-web-converter.sh 2368" &&

echo "./feature-image-location-fixer.sh" &&

echo "./gitpush.sh"
}

function fireoff(){
 systemctl stop firewalld &&
 sudo ufw allow 1714:1764/udp &&
 sudo ufw reload

}

function paste() {
              local file=${1:-/dev/stdin}
              curl --data-binary @${file} https://paste.rs
}
alias pb="nc termbin.com 9999"

function awei(){
bluetoothctl connect 45:A6:63:27:9F:2F

echo "power on  |  agent on  |  list"
echo "devices    | default-agent    |    trust A0:E9:AF:10:62:3F    |    pair A0:E9:AF:10:62:3F    |    connect A0:E9:AF:10:62:3F "
}


ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
