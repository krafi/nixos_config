# sudo ln -s /run/current-system/sw/bin/bash /bin/bash  # if you want to use ./
# sudo nixos-rebuild switch

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  # boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  networking.hostName = "nixos"; 
  nixpkgs.config.allowUnfree = true;
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  # networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Asia/Dhaka";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  services.xserver.displayManager.lightdm.enable = true;
  services.devmon.enable = true;
  services.gvfs.enable = true;
  services.udisks2.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };
  
  services.xserver.windowManager = {	
        i3.enable = true;
  };


  # Enable sound.
   #sound.enable = true;
   #hardware.pulseaudio.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.krafi = {
    isNormalUser = true;
    description = "krafi";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
    #  firefox
    #  thunderbird
    ];
  };

  # Enable automatic login for the user.
  # services.xserver.displayManager.autoLogin.enable = true;
  # services.xserver.displayManager.autoLogin.user = "krafi";


   security.polkit.enable = true;
  environment.systemPackages = with pkgs; [
    automake
    bleachbit
    cudatext
    copyq
    curl
    firefox
    dunst
    dex
    feh
    flameshot
    font-awesome
    glib
    gImageReader
    htop
    i3
    tesseract
    lf
    libsForQt5.ffmpegthumbs
    lxappearance
    git
    guake
    gcc
    mpv
    neovim
    nettools
    neofetch
    nitrogen
    nginx
    networkmanagerapplet
    pavucontrol
    polkit
    polkit_gnome
    pcmanfm
    picom
    rofi
    starship
    sxhkd
    scrot
    slock
    tilda # for i3
    udisks2
    udiskie
    unrar
    unzip
    volumeicon
    wget
    xorg.xkill
    xdotool
    zsh
   
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

 programs.dconf.enable = true;

 #programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  programs.zsh = {
    enable = true;
    shellAliases = {
      vim = "nvim";
    };
    enableCompletion = true;
    #enableAutosuggestions = true;
    autosuggestions.enable = true;
    interactiveShellInit = ''
      #source ${pkgs.fetchurl {url = "https://github.com/rupa/z/raw/2ebe419ae18316c5597dd5fb84b5d8595ff1dde9/z.sh"; sha256 = "0ywpgk3ksjq7g30bqbhl9znz3jh6jfg8lxnbdbaiipzgsy41vi10";}}
      export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh
      export ZSH_THEME="lambda"
      plugins=(git)
      source $ZSH/oh-my-zsh.sh
      PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"
      # Auto completion / suggestion -> Fish-like suggestion for command history
	  source ~/.local/custom/zsh-autosuggestions/zsh-autosuggestions.zsh

	  #source ~/zsh-autocomplete/zsh-autocomplete.plugin.zsh
	  source ~/.oh-my-zsh/mini-oh-my-zsh.sh

	  # Fish like syntax highlighting
	  source ~/.local/custom/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
		
   	  # Save type history for completion and easier life
	  HISTFILE=~/.zsh_history
	  HISTSIZE=99999
	  SAVEHIST=99999
	  setopt appendhistory

	  # Select all suggestion instead of top on result only
	  zstyle ':autocomplete:tab:*' insert-unambiguous yes
	  zstyle ':autocomplete:tab:*' widget-style menu-select
	  zstyle ':autocomplete:*' min-input 2

      neofetch
	  eval "$(starship init zsh)"
    '';
    promptInit = "";
  };
  
  systemd = {
  user.services.polkit-gnome-authentication-agent-1 = {
    description = "polkit-gnome-authentication-agent-1";
    wantedBy = [ "graphical-session.target" ];
    wants = [ "graphical-session.target" ];
    after = [ "graphical-session.target" ];
    serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
  };
 };
  xdg.portal = {
    enable = true;
    wlr.enable = true;
  };

  # Enable the OpenSSH daemon.
   services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;


  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;



  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

 swapDevices = [ {
     device = "/var/lib/swapfile";
     size = 9*1024;
     ##randomEncryption.enable = true; 
   } ];





 }





