# sudo ln -s /run/current-system/sw/bin/bash /bin/bash  # if you want to use ./
# sudo nixos-rebuild switch

{ config, pkgs,xmonad-contexts, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  # VirtualBox
   virtualisation.virtualbox.host.enable = true;
   users.extraGroups.vboxusers.members = [ "user-with-access-to-virtualbox" ];
   nixpkgs.config.allowUnfree = true;
   #virtualisation.virtualbox.host.enable = true;
   virtualisation.virtualbox.host.enableExtensionPack = true;
  #ntfs support
  boot.supportedFilesystems = [ "ntfs" ];
  

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Asia/Dhaka";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  #services.xserver.displayManager.gdm.enable = true;
  #services.xserver.desktopManager.gnome.enable = true;



  services.xserver.displayManager.lightdm.enable = true;
  services.devmon.enable = true;
  services.gvfs.enable = true;
  services.udisks2.enable = true;




  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };
  
  services.xserver.windowManager = {
	    leftwm.enable = true;
  		spectrwm.enable = true;
  		bspwm.enable = true;
  		xmonad.enable = true;
  		xmonad.enableContribAndExtras = true;
  		    xmonad.ghcArgs = [
      "-hidir /tmp" # place interface files in /tmp, otherwise ghc tries to write them to the nix store
      "-odir /tmp" # place object files in /tmp, otherwise ghc tries to write them to the nix store
      "-i${xmonad-contexts}" # tell ghc to search in the respective nix store path for the module
    ];
  		
        i3.enable = true;
  };

#inputs.xmonad-contexts = {
#  url = "github:Procrat/xmonad-contexts";
#  flake = false;
#};






  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.krafi = {
    isNormalUser = true;
    description = "krafi";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
    #  firefox
    #  thunderbird
    ];
  };

  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "krafi";

  # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
  systemd.services."getty@tty1".enable = true;
  systemd.services."autovt@tty1".enable = true;
  
  #Kdeconnect
  programs.kdeconnect.enable = true;


  # Enable OpenTabletDriver
  # you may need to enable unstable branch for using opentabletDriver
  # https://github.com/OpenTabletDriver/OpenTabletDriver/issues/2925#issuecomment-1712730036
   hardware.opentabletdriver.enable = true;
   services.udev.extraRules = "";
   security.polkit.enable = true;
  environment.systemPackages = with pkgs; [
    automake
    bleachbit
    blueman
    cudatext
    copyq
    curl
    firefox
    dunst
    dex
    doublecmd
    feh
    flameshot
    font-awesome
    #fastfetch #unstable brunch
    ferdium
    gimp
    glib
    gImageReader
    gparted
    htop
    handbrake
    haskellPackages.xmonad-contrib
    haskellPackages.xmonad
    i3
    inkscape
    konsole
    krita
    lf
    libsForQt5.dolphin
    libsForQt5.kio-extras
    libsForQt5.kdeconnect-kde
    libsForQt5.qt5.qtbase
    libsForQt5.kdegraphics-thumbnailers
    libsForQt5.ffmpegthumbs
    libsForQt5.kdenlive
    librewolf
    lxappearance
    git
    guake # for xmonand
    gcc
    mpv
    neovim
    neofetch
    nitrogen
    networkmanagerapplet
   # opentabletdriver
    obs-studio
    partition-manager
    pavucontrol
    polybar
    polkit
    polkit_gnome
    pcmanfm
    python3
    python3Packages.virtualenv
    picom
    qbittorrent
    rofi
    starship
    sqlite
    sxhkd
    slock
    signal-cli
    tilda # for i3
    udisks2
    udiskie
    unrar
    unzip
    tmux
    vivaldi
   # veikk-linux-driver-gui
   
    volumeicon
    vim 
    wget
    xorg.xkill
    xmobar
    xdotool
    xmonad-with-packages
    zsh
    zoom-us
   
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

programs.dconf.enable = true;

 #programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  programs.zsh = {
    enable = true;
    shellAliases = {
      vim = "nvim";
    };
    enableCompletion = true;
    #enableAutosuggestions = true;
    autosuggestions.enable = true;
    interactiveShellInit = ''
      #source ${pkgs.fetchurl {url = "https://github.com/rupa/z/raw/2ebe419ae18316c5597dd5fb84b5d8595ff1dde9/z.sh"; sha256 = "0ywpgk3ksjq7g30bqbhl9znz3jh6jfg8lxnbdbaiipzgsy41vi10";}}
      export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh
      export ZSH_THEME="lambda"
      plugins=(git)
      source $ZSH/oh-my-zsh.sh
      PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"
      # Auto completion / suggestion -> Fish-like suggestion for command history
	  source ~/.local/custom/zsh-autosuggestions/zsh-autosuggestions.zsh

	  #source ~/zsh-autocomplete/zsh-autocomplete.plugin.zsh
	  source ~/.oh-my-zsh/mini-oh-my-zsh.sh

	  # Fish like syntax highlighting
	  source ~/.local/custom/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
		
   	  # Save type history for completion and easier life
	  HISTFILE=~/.zsh_history
	  HISTSIZE=99999
	  SAVEHIST=99999
	  setopt appendhistory

	  # Select all suggestion instead of top on result only
	  zstyle ':autocomplete:tab:*' insert-unambiguous yes
	  zstyle ':autocomplete:tab:*' widget-style menu-select
	  zstyle ':autocomplete:*' min-input 2

      neofetch
	  eval "$(starship init zsh)"
    '';
    promptInit = "";
  };
  
  systemd = {
  user.services.polkit-gnome-authentication-agent-1 = {
    description = "polkit-gnome-authentication-agent-1";
    wantedBy = [ "graphical-session.target" ];
    wants = [ "graphical-session.target" ];
    after = [ "graphical-session.target" ];
    serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
  };
};
  xdg.portal = {
    enable = true;
    wlr.enable = true;
  };
  # Flatpak
  services.flatpak.enable = true;
  # Run this command on terminal :
  # flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    
    
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
   services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

swapDevices = [ {
    device = "/var/lib/swapfile";
    size = 9*1024;
    #randomEncryption.enable = true; 
  } ];

}
#sudo nixos-rebuild switch
